#include <stdio.h>

// cette fonction calcule le resultat de x^y
int puiss(int x, int y){
    int res = 1;
    while(y >= 1){
        res *= x;
        y--;
    }
    return res;
}

// Cette fonction affichera un systeme lineaire ainsi que les valeurs du vecteur X
void AfficheMatrice(int n, float A[][n], float * B, float * X){
    int i;
    for(i = 0; i < n; i++){
        int j;
        for(j = 0; j < n; j++){
            printf("%f ",A[i][j]);
        }
        printf("| %f ", B[i]);
        printf("--> %f", X[i]);
        printf("\n");
    }
    printf("\n");
}

// Cette fonction va effectuer une triangularisation superieur de la matrice 
// representant le systeme lineaire AX = B a l'aide de la methode de Gauss
void triangularisation(int n, float  A[][n], float * B){
    float pivot;

    int k;
    // Parcourt de la premiere colonne a l'avant derniere colonne pour mettre des 0 en-dessous
    // de chaque coefficient
    for(k = 0; k < n-1; k++){
        int i;
        // Parcourt de la matrice de la ligne suivante a la derniere ligne pour calculer le pivot
        // pour chaque ligne
        for(i = k+1; i < n; i++){
            // Calcul du pivot
            pivot = A[i][k] / A[k][k];
            int j;
            // Parcourt de la matrice de la colonne actuelle a la derniere colonne pour appliquer
            // la fonction avec le pivot sur chaque valeur de la ligne            
            for(j = k; j < n; j++){
                // Modification de la valeur de a(ij) de A a partir du pivot
                A[i][j] = A[i][j] - pivot * A[k][j];
            }
            // Modification de la valeur de la ligne i de B
            B[i] = B[i] - pivot * B[k];
        }
    }
}

// Cette fonction va effectuer la resolution d'un systeme lineaire a partir
// d'une matrice triangulaire superieur
void resolution(int n, float A[][n], float * B, float * X){
    // On calcule l'inconnue la plus facile soit celle situee en derniere ligne
    // et derniere colonne
    X[n-1] = B[n-1] / A[n-1][n-1];

    float somme = 0;

    int i;
    // Parcourt de la matrice de l'avant-derniere ligne a la premiere
    for(i = n-2; i >= 0; i--){
        int j;
        somme = 0;
        // Pour chaque colonne, on calcule la somme des produits de la valeur
        // dans A(i,j) et de la valeur dans X(j)
        for(j = i+1; j < n; j++){
            somme += A[i][j] * X[j];
        }
        // On resout l'equation en effectuant le produit de la difference de 
        // la valeur de B(i) et de la somme calculee par la valeur de A(i,i)
        X[i] = (B[i] - somme) / A[i][i];
    }
}

// Fonction d'affichage global
void test(int n, float A[][n], float * B, float * X){
    AfficheMatrice(n, A, B, X);
    triangularisation(n, A, B);
    resolution(n, A, B, X);
    AfficheMatrice(n, A, B, X);
    printf("\n\n");
}


// Pour tester les 4 premieres matrices, il faut decommenter de la ligne 94 a 125
// et mettre en commentaire la ligne 126 a 181.
int main(){
    // Creation des matrices
    float a1[3][3] = {{3, 0, 4}, {7, 4, 2}, {-1, 1, 2}};
    float b1[3] = {7, 13, 2};

    float a2[3][3] = {{-3, 3, -6}, {-4, 7, 8}, {5, 7, -9}};
    float b2[3] = {-6, 11, 3};

    float a3[3][3] = {{4, 1, 1}, {2, -9, 0}, {0, -8, 6}};
    float b3[3] = {6, -7, -2};

    float a4[3][3] = {{7, 6, 9}, {4, 5, -4}, {-7, -3, 8}};
    float b4[3] = {22, 5, -2};



    float X[3] = {0, 0, 0};

    // Premiere matrice
    test(3, a1, b1, X);

    // on remet X a (0, 0, 0) pour montrer qu'on revient bien a (1, 1, 1)
    X[0] = 0; X[1] = 0; X[2] = 0; 
    // Deuxieme matrice
    test(3, a2, b2, X);

    X[0] = 0; X[1] = 0; X[2] = 0;
    // Troisieme matrice
    test(3, a3, b3, X);

    X[0] = 0; X[1] = 0; X[2] = 0;
    // Quartieme matrice
    test(3, a4, b4, X);
    
    
    // creation de la matrice a5
    float a5[10][10];
    float b5[10];
    float X5[10];
    int i;
    for(i = 0; i < 10; i++){
        b5[i] = 0;
        int j;
        for(j = 0; j < 10; j++){
            if(i == j){
                a5[i][j] = 1;
            }
            else if(i == 0 || j == 0){
                a5[i][j] = puiss(2, (1-j));
            }
            else{
                a5[i][j] = 0;
            }
            b5[i] += a5[i][j];
        }
        X5[i] = 0;
    }

    test(10, a5, b5, X5);


    float a6[10][10];
    float b6[10];
    float X6[10];
    int k;
    for(k = 0; k < 10; k++){
        b6[k] = 0;
        int o;
        for(o = 0; o < 10; o++){
            if(k == o){
                a6[k][o] = 3;
            }
            else if(o == k+1 && k < 10-1){
                a6[k][o] = -1;
            }
            else if(o == k-1 && k > 0){
                a6[k][o] = -2;
            }
            else{
                a6[k][o] = 0;
            }
            b6[k] += a6[k][o];
        }
        X6[k] = 0;
    }

    test(10, a6, b6, X6);

    

    

    return 0;
}
