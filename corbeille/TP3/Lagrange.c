#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


//fonction prend en argument la liste X donnée dans les jeux d'essais, la taille de cette liste, l'itération i pour 
//laquelle la func card est calculée et la valeur x arbitraire
//renvoie la fonction cardinale à l'itération i voulue
//pour chaque itération de la liste X (exceptée l'itération égale à i) on calcule le quotient donné par la formule
//qui sera multiplié par les autres quotients calculés pour obtenir la func cardinale li 
float func_card(float * X, int taille, int i, int x){
    int N = taille;
    float prod = 1;

    //printf("\n");
    for(int j=0;j<N;j++){
        if(j!=i){
            //printf("%f ! \n",(x - X[j])/(X[i] - X[j]));
            prod = prod * ((x - X[j])/(X[i] - X[j]));
            //printf("%f\n",prod);
        }
    }
    //printf("\n");
    return prod;
}

//fonction prend en argument la liste Y donnée par les jeux d'essais, la liste li des fonctions cardinales et la taille de la liste Y
//renvoie la somme des produits des valeurs des fonctions cardinales par les valeurs de la liste Y
float P(float * Y, float * li, int taille){
    int N = taille;
    float somme = 0;

    for(int i=0;i<N;i++){
        somme += Y[i] * li[i];
    }

    return somme;
}

//prend en argument une liste X, sa taille et un entier choix
void creerX(float * X, int taille, int choix){

    if(choix == 1){
        FILE * f1 = fopen("TabX1.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille-1;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 2){
        FILE * f1 = fopen("TabX2.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille-1;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 3){
        FILE * f1 = fopen("TabX3.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille-1;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }
}


void creerY(float * Y, int taille, int choix){

    if(choix == 1){
        FILE * f1 = fopen("TabY1.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille-1;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 2){
        FILE * f1 = fopen("TabY2.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille-1;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 3){
        FILE * f1 = fopen("TabY3.txt","r");

        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille-1;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }
}



int main(){

    int x = 1;
    int choix = 0;
    printf("Quel test désirez-vous ? ");
    scanf("%d",&choix);
    printf("Quel valeur pour x ? ");
    scanf("%d",&x);



    if(choix == 1){

        int taille = 20;
        
        float * X = malloc(taille*sizeof(int));
        creerX(X, taille, 1);

        float * Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 1);
        
        //allocation de la liste li qui stockera les valeurs des fonctions cardinales
        float * li = malloc(taille*sizeof(float));

        for(int i=0;i<taille-1;i++){
            li[i]=func_card(X, taille, i, x);
        }

        printf("Le résultat est %f\n",P(Y, li, taille));
        free(X);
        free(Y);
        free(li);
    }

    else if(choix == 2){
         
        int taille = 21;

        float * X = malloc(taille*sizeof(int));
        creerX(X, taille, 2);

        float * Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 2);

        //allocation de la liste li qui stockera les valeurs des fonctions cardinales
        float * li = malloc(taille*sizeof(float));

        for(int i=0;i<taille-1;i++){
            li[i]=func_card(X, taille, i, x);
            printf("%f! ",li[i]);
        }

        printf("Le résultat est %f\n",P(Y, li, taille));
        free(X);
        free(Y);
        free(li);
    }

    else if(choix == 3){

        int taille = 11;

        float * X = malloc(taille*sizeof(int));
        creerX(X, taille, 3);

        float * Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 3);

        //allocation de la liste li qui stockera les valeurs des fonctions cardinales
        float * li = malloc(taille*sizeof(float));

        for(int i=0;i<taille-1;i++){
            li[i]=func_card(X, taille, i, x);
        }

        printf("Le résultat est %f\n",P(Y, li, taille));
        free(X);
        free(Y);
        free(li);
    }

    else{
        printf("Erreur recommencer\n");
    }

    
    return 0;
}
