import matplotlib.pyplot as plt
import numpy as np

###################
print("MATRICE A1")
###################

plt.figure()
x = np.linspace(0, 2, 400)

print("Jacobi")

nbIte = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
residu = [1.0, 2.376093, 6.269128, 31.237606, 70.131203, 226.017288, 1047.708008, 3893.932373, 8509.089844, 35010.335938, 83796.843750]

plt.plot(nbIte, residu, color="blue", label = "Jacobi")

plt.xlabel("Nombre d'itérations")
plt.ylabel("Erreur relative")

plt.title("Matrice A1")
plt.legend(loc=0)

plt.show()

plt.figure()
x = np.linspace(0, 2, 400)

print("Gauss-Seidel")

nbIte = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
residu = [1.0, 114.654877, 11353.144531, 1124190.0, 111317432.0, 11022671872.0, 1091466428416.0, 108077156139008.0, 10701815108599808.0, 1059694875760394240.0, 104931126936709627904.0]

plt.plot(nbIte, residu, color="red", label = "Gauss-Seidel")

plt.title("Matrice A1")
plt.legend(loc=0)

plt.show()

###################
print("MATRICE A2")
###################

plt.figure()
x = np.linspace(0, 2, 400)

print("Jacobi")

nbIte = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
residu = [1.0, 38.978909, 1248.130615, 32709.902344, 1421016.1250, 59182448.0, 1855734400.0, 49933156352.0, 1419458510848.0, 48691113099264.0, 1382659762683904.0]

plt.plot(nbIte, residu, color="blue", label = "Jacobi")

plt.title("Matrice A2")
plt.legend(loc=0)

plt.show()

plt.figure()
x = np.linspace(0, 2, 400)

print("Gauss-Seidel")

nbIte = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
residu = [1.0, 956.377502, 979330.56250, 1002834624.0, 1026902654976.0, 1051548318695424.0, 1076785478344114176.0, 1102628329824372916224.0, 1129091409740157866213376.0, 1156189603573921655002497024.0, 1183938154059695774722556952576.0]

plt.plot(nbIte, residu, color="red", label = "Gauss-Seidel")

plt.title("Matrice A2")
plt.legend(loc=0)

plt.show()

###################
print("MATRICE A3")
###################

plt.figure()
x = np.linspace(0, 2, 400)

print("Jacobi")

nbIte = []
for i in range(14):
    nbIte.append(i)
    
residu = [1, 1.333333, 0.388889, 0.148148, 0.115226, 0.031379, 0.017375, 0.009297, 0.002334, 0.001804, 0.000691, 0.000155, 0.000172, 0.000052]

plt.plot(nbIte, residu, color="blue", label = "Jacobi")

print("Gauss-Seidel")

residu = [1.0, 0.500000, 0.064815, 0.008402, 0.001089, 0.000141, 0.000018, 0.000018, 0.000018, 0.000018, 0.000018, 0.000018, 0.000018, 0.000018]

plt.plot(nbIte, residu, color="red", label = "Gauss-Seidel")

plt.title("Matrice A3")
plt.legend(loc=0)

plt.show()

###################
print("MATRICE A4")
###################

plt.figure()
x = np.linspace(0, 2, 400)

print("Jacobi")

nbIte = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38]
residu = [1.0, 2.714286, 0.683036, 0.113176, 0.109411, 0.049026, 0.017777, 0.005031, 0.002890, 0.001514, 0.000430, 0.000161, 0.000097, 0.000097, 0.000097, 0.000097, 0.000097, 0.000097, 0.000097, 0.000097]

plt.plot(nbIte, residu, color="blue", label = "Jacobi")

print("Gauss-Seidel")

residu = [1.0, 0.956633, 0.832723, 0.604311, 0.388695, 0.245118, 0.153054, 0.086129, 0.043024, 0.018010, 0.006483, 0.006269, 0.005250, 0.003725, 0.002354, 0.001503, 0.000923, 0.000510, 0.000249, 0.000099]

plt.plot(nbIte, residu, color="red", label = "Gauss-Seidel")

plt.title("Matrice A4")
plt.legend(loc=0)

plt.show()

###################
print("MATRICE A5 de Taille 10")
###################

plt.figure()
x = np.linspace(0, 2, 400)

print("Jacobi")

nbIte = []
for i in range(0,19):
    nbIte.append(i)
    
residu = [1.0, 0.998047, 0.499023, 0.332681, 0.166341, 0.110893, 0.055447, 0.036964, 0.018482, 0.012321, 0.006161, 0.004107, 0.002054, 0.001369, 0.000684, 0.000456, 0.000228, 0.000152, 0.000076]

plt.plot(nbIte, residu, color="blue", label = "Jacobi")

print("Gauss-Seidel")

residu = [1.0, 0.998047, 0.332681, 0.110893, 0.036964, 0.012321, 0.004107, 0.001369, 0.000456, 0.000152, 0.000051, 0.000051, 0.000051, 0.000051, 0.000051, 0.000051, 0.000051, 0.000051, 0.000051]

plt.plot(nbIte, residu, color="red", label = "Gauss-Seidel")

plt.title("Matrice A5")
plt.legend(loc=0)

plt.show()

###################
print("MATRICE A6 de Taille 10")
###################

plt.figure()
x = np.linspace(0, 2, 400)

print("Jacobi")

nbIte = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105]
residu = [1.0, 0.995885, 0.812071, 0.610076, 0.398034, 0.255690, 0.159129, 0.097011, 0.059695, 0.035916, 0.022035, 0.013211, 0.008099, 0.004852, 0.002974, 0.001781, 0.001091, 0.000654, 0.000401, 0.000240, 0.000147, 0.000098]

plt.plot(nbIte, residu, color="blue", label = "Jacobi")

print("Gauss-Seidel")

residu = [1.0, 0.554754, 0.220099, 0.082444, 0.030421, 0.011181, 0.004105, 0.001507, 0.000553, 0.000203, 0.000091, 0.000091,0.000091,0.000091,0.000091,0.000091,0.000091,0.000091,0.000091,0.000091,0.000091,0.000091]

plt.plot(nbIte, residu, color="red", label = "Gauss-Seidel")

plt.title("Matrice A6")
plt.legend(loc=0)

plt.show()
