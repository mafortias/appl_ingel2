#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "annexe.h"

/* Cette fonction prend entree un vecteur X de taille n et renvoie
la difference maximale entre 1 et une une de ses valeurs */ 
float erreurRelative(int n,float X[n]){
    float max = 0; // valeur la plus petite
    int i;
    for(i = 0; i < n; i++){ // parcourt du vecteur
        // on regarde si la difference 1-X[i] est superieur a la difference
        // maximale entre 1-X[0], ..., 1-X[i-1]
        if(valeurAbsolue(1-X[i]) > max){
            max = valeurAbsolue(1-X[i]);
        }
    }
    return max;
}

//Cette fonction prend en argument une matrice carrée et sa taille. Elle retourne 1 si A est sym 0 sinon
int Sym(int n, float A[][n]){
    //réponse initialisée à Vrai
    int verif = 1;

    //parcourt de chaque valeurs de la matrice A
    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){

            //si la valeur en cours est différente de sa valeur symétrique par rapport à la diagonale de la matrice
            //alors on met la réponde à Faux
            if(A[i][j]!=A[j][i]){
                verif=0;
            }
        }
    }
    return verif;
}

/* Cette fonction prend en entree une matrice A de taille n et renvoie 1 si 
la matrice est a diagonale strictement dominante et 0 sinon */
int diagonaleStrictDom(int n, float A[][n]){
    int booleen = 1; // on initialise a vrai
    int i = 0;
    int j;
    int sommeLigne = 0;
    // tant que vraie et derniere ligne de la matrice non atteinte
    while(i < n && booleen == 1){ 
        sommeLigne = 0;
        for(j = 0; j < n; j++){
            if(j != i){
                // somme des termes qui ne sont pas sur la diagonale
                sommeLigne += valeurAbsolue(A[i][j]);
            }
        }
        // si somme des termes de la ligne qui ne sont pas sur 
        // la diagonale > valeur de la ligne sur la diagonale
        if(valeurAbsolue(A[i][i]) <= sommeLigne){
            booleen = 0; // on passe a faux
        }
        i += 1;
    }
    return booleen;
}

/* La fonction Jacobi prend en entree une matrice A de taille n x n,
 une matrice X et une matrice B de taille n x 1, un flottant e qui
 correspond a la precison voulue et un entier k qui correspond au
 nombre d'iterations voulues. Elle modifie X de sorte a obtenir 
 une solution la plus proche possible de la solution exacte */
void Jacobi(int n, float A[][n], float X[n], float B[n], float e, int k){
    int i, j, k0 = 0;
    float somme;

    /* calcul de l'erreur relative au rang 0 qui est egale a 1 
    dans notre cas car le vecteur X initial est X = {0, 0, 0} 
    (donc 1 - 0 = 1) */
    float e0 = erreurRelative(n, X);
    printf("Erreur relative a  %d-ieme iteration : %f\n", k0, erreurRelative(n, X));

    float Xsuiv[n];
    int l;
    for(l = 0; l < n; l++){
        Xsuiv[l] = 0;
    }

    k0 = 1;
    /* tant que la precision et le nbre d'iterations n'est pas depasse */
    while (e0 >= e && k0 < k){
        // on boucle sur toutes les lignes de A
        for (i = 0; i < n; ++i) {
            somme = 0;
            // on boucle sur toutes les colonnes de A
            for (j = 0; j < n; ++j){
                // si on est pas sur un element de la diagonale
                if (i != j) {
                    /* on ajoute a la somme le produit entrede l'element de A en i-eme
                    ligne et j-eme colonne et l'element de X en j-eme ligne */
                    somme += A[i][j] * X[j];
                }
            }
            /* l'element a la ligne i de X devient le quotient de (l'element de B en ligne 
            i multiplie pa rla somme caclculee precedemment) par l'element de la diagonale
            de A en ligne i */
            Xsuiv[i] = (B[i] - somme) / A[i][i];
        }
        int o;
        for(o = 0; o < n; o++){
            X[o] = Xsuiv[o];
        }

        /* calcul de la nouvelle erreur relative car la matrice X a changee */
        e0 = erreurRelative(n, X);
        /* affichage de l'erreur relative a la k-ieme iteration */
        printf("Erreur relative a  %d-ieme iteration : %f\n", k0, erreurRelative(n, X));
        ++k0;
    }
    printf("\n");
}

/* La fonction Gauss-Seidel prend en entree une matrice A de taille n x n,
 une matrice X et une matrice B de taille n x 1, un flottant e qui
 correspond a la precison voulue et un entier k qui correspond au
 nombre d'iterations voulues. Elle modifie X de sorte a obtenir 
 une solution la plus proche possible de la solution exacte */
void GaussSeidel(int n, float A[][n], float X[n], float B[n], float e, int k){
    int i, j, o;
    float somme1, somme2;
    int k0 = 0;

    /* calcul de l'erreur relative au rang 0 qui est egale a 1 
    dans notre cas car le vecteur X initial est X = {0, 0, 0} 
    (donc 1 - 0 = 1) */
    float e0 = erreurRelative(n, X);
    printf("Erreur relative a  %d-ieme iteration : %f\n", k0, erreurRelative(n, X));

    k0 = 1;
    /* tant que la precision et le nbre d'iterations n'est pas depasse */
    while(e0 >= e && k0 < k){

        // on boucle sur toutes les lignes de A
        for(i = 0; i < n; i++){

            //calcul de la première somme pour les valeurs de Xj avec j allant de 0 à i
            //on ajoute à la somme le produit de la valeur en (i,j) de la matrice A par la valeur de Xj
            somme1 = 0;
            for(j = 0; j < i; j++){
                somme1 += A[i][j] * X[j];
            }

            //calcul de la deuxième somme pour les valeurs de Xj pour j allant de i+1 à n (nombre de colonnes de A)
            //on ajoute à la somme le produit de la valeur en (i,j) de la matrice A par la valeur de Xj
            somme2 = 0;
            for(o = i+1; o < n; o++){
                somme2 += A[i][o] * X[o];
            }

            //on calcule à partir des sommes obtenues la nouvelle valeur de X de coordonée i
            //d'après la formule du cours on soustrait à la val de Bi la somme calculée précédement 
            //le tout divisé par la valeur de A aux coordonnées (i,i)
            X[i] = (B[i] - somme1 - somme2) / A[i][i];
        }

        /* calcul de la nouvelle erreur relative car la matrice X a changee */
        e0 = erreurRelative(n, X);

        /* affichage de l'erreur relative a la k-ieme iteration */
        printf("Erreur relative a  %d-ieme iteration : %f\n", k0, erreurRelative(n, X));

        ++k0;
    }
    printf("\n");
}




// Fonction d'affichage global pour Jacobi
void testJacobi(int n, float A[][n], float * B, float * X){
    printf("Jacobi : \n\n");
    if(diagonaleStrictDom(n, A) == 1){
        printf("La matrice est diagonale strictement dominante.\n\n");
    }
    else{
        printf("La matrice n'est pas diagonale strictement dominante.\n\n");
    }
    afficheSysteme(n, A, B, X);
    Jacobi(n, A, X, B, 0.0001, 200);
    afficheSysteme(n, A, B, X);
    printf("\n\n");
}

// Fonction d'affichage global pour Gauss-Seidel
void testGaussSeidel(int n, float A[][n], float * B, float * X){
    printf("GaussSeidel :\n\n");
    if(Sym(n, A) == 1){
        printf("La matrice est symétrique.\n\n");
    }
    else{
        printf("La matrice n'est pas symétrique.\n\n");
    }
    afficheSysteme(n, A, B, X);
    GaussSeidel(n, A, X, B, 0.0001, 200);
    afficheSysteme(n, A, B, X);
    printf("\n\n");
}



int main(){

    float X[3] = {0, 0, 0};
    int choixMat = 0;
    printf("Quelle matrice (1, 2, 3, 4, 5 ou 6) ? \n");
    scanf("%d", &choixMat);
    if(choixMat == 1){
        // Premiere matrice
        printf("########################\n### Premiere matrice ###\n########################\n\n");
        float a1[3][3] = {{3, 0, 4}, {7, 4, 2}, {-1, 1, 2}};
        float b1[3] = {7, 13, 2};
        testJacobi(3, a1, b1, X);
        X[0] = 0; X[1] = 0; X[2] = 0; 
        testGaussSeidel(3, a1, b1, X);
    }


    else if(choixMat == 2){
        // Deuxieme matrice
        printf("########################\n### Deuxieme matrice ###\n########################\n\n");
        float a2[3][3] = {{-3, 3, -6}, {-4, 7, 8}, {5, 7, -9}};
        float b2[3] = {-6, 11, 3};
        testJacobi(3, a2, b2, X);
        X[0] = 0; X[1] = 0; X[2] = 0; 
        testGaussSeidel(3, a2, b2, X);
    }


    else if(choixMat == 3){
        // Troisieme matrice
        printf("#########################\n### Troisieme matrice ###\n#########################\n\n");
        float a3[3][3] = {{4, 1, 1}, {2, -9, 0}, {0, -8, 6}};
        float b3[3] = {6, -7, -2};
        testJacobi(3, a3, b3, X);
        X[0] = 0; X[1] = 0; X[2] = 0; 
        testGaussSeidel(3, a3, b3, X);
    }


    else if(choixMat == 4){
        // Quartieme matrice
        printf("#########################\n### Quatrieme matrice ###\n#########################\n\n");
        float a4[3][3] = {{7, 6, 9}, {4, 5, -4}, {-7, -3, 8}};
        float b4[3] = {22, 5, -2};
        testJacobi(3, a4, b4, X);
        X[0] = 0; X[1] = 0; X[2] = 0; 
        testGaussSeidel(3, a4, b4, X);
    }

        
    else if(choixMat == 5){
        // Cinquieme matrice
        printf("#########################\n### Cinquieme matrice ###\n#########################\n\n");
        int n5;
        printf("Quelle taille (3, 6, 8 ou 10) ?\n");
        scanf("%d", &n5);
        float a5[n5][n5];
        float b5[n5];
        float X5[n5];
        int i;
        for(i = 0; i < n5; i++){
            b5[i] = 0;
            int j;
            for(j = 0; j < n5; j++){
                if(i == j){
                    a5[i][j] = 1;
                }
                else if(i == 0){
                    a5[i][j] = pow(2, -j);
                }
                else if(j == 0){
                    a5[i][j] = pow(2, -i);
                }
                else{
                    a5[i][j] = 0;
                }
                b5[i] += a5[i][j];
            }
            X5[i] = 0;
        }
        testJacobi(n5, a5, b5, X5);
        int l;
        for(l = 0; l < n5; l++){
            X5[l] = 0;
        }
        testGaussSeidel(n5, a5, b5, X5);
    }


    else{
        // Sixieme matrice
        printf("#######################\n### Sixieme matrice ###\n#######################\n\n");
        int n6;
        printf("Quelle taille (3, 6, 8 ou 10) ?");
        scanf("%d", &n6);
        float a6[n6][n6];
        float b6[n6];
        float X6[n6];
        int k;
        for(k = 0; k < n6; k++){
            b6[k] = 0;
            int o;
            for(o = 0; o < n6; o++){
                if(k == o){
                    a6[k][o] = 3;
                }
                else if(o == k+1 && k < n6-1){
                    a6[k][o] = -1;
                }
                else if(o == k-1 && k > 0){
                    a6[k][o] = -2;
                }
                else{
                    a6[k][o] = 0;
                }
                b6[k] += a6[k][o];
            }
            X6[k] = 0;
        }
        testJacobi(n6, a6, b6, X6);
        int m;
        for(m = 0; m < n6; m++){
            X6[m] = 0;
        }
        testGaussSeidel(n6, a6, b6, X6);
    }


    return 0;
}
