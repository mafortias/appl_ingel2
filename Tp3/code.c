#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "annexe.h"

/* La fonction prend en argument la liste X donnee dans les jeux d'essais, la taille
de cette liste, l'iteration i pour laquelle la func card est calculee et la valeur x
arbitraire. Elle renvoie la fonction cardinale a l'iteration i voulue pour chaque iteration
de la liste X (exceptee l'iteration egale a i) on calcule le quotient donne par la
formule qui sera multiplie par les autres quotients calcules pour obtenir la func
cardinale li. */
float func_card(float * X, int taille, int i, int x){
    int N = taille;
    float prod = 1;

    for(int j=0;j<N;j++){
        if(j!=i){
            prod = prod * ((x - X[j])/(X[i] - X[j]));
        }
    }
    return prod;
}

/* La fonction prend en argument la liste Y donnee par les jeux d'essais, la liste
li des fonctions cardinales et la taille de la liste Y. Elle renvoie la somme des
produits des valeurs des fonctions cardinales par les valeurs de la liste Y. */
float P(float * Y, float * li, int taille){
    int N = taille;
    float somme = 0;

    for(int i=0;i<N;i++){
        somme += Y[i] * li[i];
    }

    return somme;
}

/*La fonction prend en argument la taille du jeu d'essais une liste de X et de Y correspondants et une valeur x pour laquelle
on calcule la valeur en appliquant le polynôme de Lagrange grâce aux fonctions func_card et P*/ 
float Lagrange(int taille, float * X, float * Y, float x){

    float * li = malloc(taille*sizeof(float));

    for(int j=0;j<taille;j++){
        li[j]=func_card(X, taille, j, x);
    }

    return(P(Y, li, taille));

}

/* Cette fonction prend en entree :
- un entier nbElem : taille du tableau fourni
- un tableau de flottants X : abscisses fournies
- un tableau de flottants Y : ordonnees fournies
- un entier x : abscisse choisi par l'utilisateur pour trouver la solution associee
Cette fonction renvoie un flottant resultat : ordonnee associee a l'abscisse choisie
*/
float Neville(int nbElem, float * X, float * Y, float x){
    float resultat;

    float * yTemp = malloc(sizeof(float) * nbElem); // tableau qui contiendra le resultat des polynomes au rang precedent (k-1)
    copieTab(nbElem, yTemp, Y); // contient les valeurs de Y au rang 0

    int n = nbElem - 1; // nb de polynomes a calculer au rang k

    float * yTempSuiv = malloc(sizeof(float) * n); // contient les valeurs des polynomes au rang k (taille = yTemp-1)

    int k = 0;

    while(n > 0){ // tant que le nb de polynome a calculer est superieur a 0
        k += 1;
        
        if(k == 1){ // a l'iteration 1
            int i;
            for(i = 0; i < n; i++){
                yTempSuiv[i] = (x - X[i+1]) * yTemp[i] + (X[i] - x) * yTemp[i+1];
                yTempSuiv[i] = yTempSuiv[i] / (X[i] - X[i+1]);
            }
        }
        else{ // a l'iteration k
            int i;
            for(i = 0; i < n; i++){
                yTempSuiv[i] = (x - X[i+k]) * yTemp[i] + (X[i] - x) * yTemp[i+1];
                yTempSuiv[i] = yTempSuiv[i] / (X[i] - X[i+k]);
            }
        }

        for(int i = 0; i < nbElem; i++){
            yTemp[i] = 0; // on remplit de 0, les cases dont l'indice > taille auront une valeur de 0
        }

        copieTab(n, yTemp, yTempSuiv);

        resultat = yTemp[0];

        for(int i = 0; i < nbElem-1; i++){
            yTempSuiv[i] = 0; // on remplit de 0, les cases dont l'indice > taille auront une valeur de 0
        }

        n--;
    }

    free(yTemp);
    free(yTempSuiv);
   
    return resultat; 
}

/* Cette fonction prend en entree :
- un entier nbElem : taille du tableau fourni
- un tableau de flottants X : abscisses fournies
- un tableau de flottants Y : ordonnees fournies
- un entier x : abscisse choisi par l'utilisateur pour trouver la solution associee
Cette fonction affiche un polynome de degre 1 correspondant a la droite de regression
lineaire associee aux donnees fournies.
*/
void RegressionLineaire(int nbElem, float * X, float * Y){
    float a1; // coefficient directeur de la droite
    float a0; // ordonnee a l'origine de la droite

    float xMoy = moyTab(nbElem, X);
    float yMoy = moyTab(nbElem, Y);

    float prodMoy = xMoy * yMoy; // produit des moyennes

    float moyProd = produitTab(nbElem, X, Y) / nbElem; // moyenne des produits

    float carreMoyX = xMoy * xMoy; // (moyenne de X) au carre

    float moyCarreX = produitTab(nbElem, X, X) / nbElem; // moyenne (de chaque elem de X au carre)

    a1 = (moyProd - prodMoy) / (moyCarreX - carreMoyX);

    a0 = yMoy - a1 * xMoy;

    printf("Droite de la regression lineaire y = %f * x + %f\n", a1, a0);
}

/* Cette fonction prend en entree :
- un entier nbElem : taille du tableau fourni
- un tableau de flottants X : abscisses fournies
- un tableau de flottants Y : ordonnees fournies
Cette fonction affiche un polynome de degre 1 correspondant a la droite de regression
lineaire associee aux donnees fournies.
*/
void ApproximationPuissance(int nbElem, float * X, float * Y){
    float a1;
    float a0;

    float * tabLnX = convertTabLn(nbElem, X); // calcul du log de chaque elem de X
    float * tabLnY = convertTabLn(nbElem, Y); // calcul du log de chaque elem de Y

    /*for(int i = 0; i < nbElem; i++){
        printf("ln(X) = %f et ln(Y) = %f\n", tabLnX[i], tabLnY[i]);
    }*/

    float sommeLnX = sommeTab(nbElem, tabLnX); // somme des elem de tabLnX
    float sommeLnY = sommeTab(nbElem, tabLnY); // somme des elem de tabLnY
    //printf("sommeLnX = %f et sommeLnY = %f\n", sommeLnX, sommeLnY);

    float * tabCarreLnX = produitElemTab(nbElem, tabLnX, tabLnX); // calcul du carre de chaque elem de tabLnX
    /*for(int i = 0; i < nbElem; i++){
        printf("tabCarreLnX = %f\n", tabCarreLnX[i]);
    }*/
    float sommeCarreLnX = sommeTab(nbElem, tabCarreLnX); // calcul de la somme de chaque elem de tabCarreLnX
    //printf("sommeCarreLnX = %f\n", sommeCarreLnX);

    float * tabProduitLnXLnY = produitElemTab(nbElem, tabLnX, tabLnY); // calcul du produit de chaque elem de tabLnX et de tabLnY
    /*for(int i = 0; i < nbElem; i++){
        printf("tabProduitLnXLnY = %f\n", tabProduitLnXLnY[i]);
    }*/
    float sommeProduitLnXLnY = sommeTab(nbElem, tabProduitLnXLnY); // calcul de la somme de chaque elem de tabProduitLnXLnY
    //printf("sommeProduitLnXLnY = %f\n", sommeProduitLnXLnY);

    // resolution du systeme a deux inconnues a1 et a0 par la methode de Gauss (cf tp1)
    float A[2][2] = {{sommeCarreLnX, sommeLnX}, {sommeLnX, nbElem}};
    float B[2] = {sommeProduitLnXLnY, sommeLnY};
    float * sol = resolutionSysteme(2, A, B);
    a1 = sol[0];
    a0 = sol[1];

    printf("y = %f * x^%f\n",exp(a0), a1); // affichage de l'equation de la courbe

    free(tabLnX);
    free(tabLnY);
    free(tabCarreLnX);
    free(tabProduitLnXLnY);
    free(sol);
}

void testLagrange(){

    int time;
    int choix;
    printf("Quel test desirez-vous ?\n");
    scanf("%d",&choix);
    float * X = NULL;
    float * Y = NULL;

    printf("####### LAGRANGE #######\n\n");

    if(choix == 1){

        int taille = 20;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 1);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 1);

        //afficheDonnees(taille, X, Y);

        //allocation de la liste li qui stockera les valeurs des fonctions cardinales
        float * li = malloc(taille*sizeof(float));


        printf("Verification sur les valeurs de l'enonce pour Lagrange : \n\n");
        
        for(int i=0; i<taille; i++){

            time=0;
            printf("x = %f    -->    Resultat attendu : %f    -->    Solution : %f\n", X[i], Y[i], Lagrange(taille, X, Y, X[i]));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        float continuer = 1;
        while(continuer != 0){
            printf("\nEntrez une valeur a tester :\n");
            scanf("%f", &continuer);
            time = 0;

            printf("Solution pour x = %f    -->    %f\n", continuer, Lagrange(taille, X, Y, continuer));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        int affichePoint;
        printf("Voulez-vous afficher tous les y associes a chaque x pour x allant de 0 a 38 par pas de 0.2 (1 : oui, 0 : non)\n");
        scanf("%d", &affichePoint);
        if(affichePoint == 1){
            float i;
            for(i = 0; i < 38.1; i += 0.2){
                printf("%f, ", Lagrange(taille, X, Y, i));
            }
        }
        printf("\n");

        free(li);

    }

    else if(choix == 2){
         
        int taille = 21;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 2);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 2);

        //afficheDonnees(taille, X, Y);

        //allocation de la liste li qui stockera les valeurs des fonctions cardinales
        float * li = malloc(taille*sizeof(float));


        printf("Verification sur les valeurs de l'enonce pour Lagrange : \n\n");
        
        for(int i=0; i<taille; i++){

            time=0;
            printf("x = %f    -->    Resultat attendu : %f    -->    Solution : %f\n", X[i], Y[i], Lagrange(taille, X, Y, X[i]));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        float continuer = 1;
        while(continuer != 0){
            printf("\nEntrez une valeur a tester :\n");
            scanf("%f", &continuer);
            time = 0;

            for(int i=0;i<taille;i++){
                li[i]=func_card(X, taille, i, continuer);
            }

            printf("Solution pour x = %f    -->    %f\n", continuer, Lagrange(taille, X, Y, continuer));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        int affichePoint;
        printf("Voulez-vous afficher tous les y associes a chaque x pour x allant de 462 a 921 par pas de 1 (1 : oui, 0 : non)\n");
        scanf("%d", &affichePoint);
        if(affichePoint == 1){
            int i;
            for(i = 462; i < 922; i += 1){
                printf("%f, ", Lagrange(taille, X, Y, i));
            }
        }
        printf("\n");

        free(li);
    }

    else if(choix == 3){

        int taille = 11;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 3);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 3);

        //afficheDonnees(taille, X, Y);

        //allocation de la liste li qui stockera les valeurs des fonctions cardinales
        float * li = malloc(taille*sizeof(float));


        printf("Verification sur les valeurs de l'enonce pour Lagrange : \n\n");
        
        for(int i=0; i<taille; i++){

            time=0;
            printf("x = %f    -->    Resultat attendu : %f    -->    Solution : %f\n", X[i], Y[i], Lagrange(taille, X, Y, X[i]));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        float continuer = 1;
        while(continuer != 0){
            printf("\nEntrez une valeur a tester :\n");
            scanf("%f", &continuer);
            time = 0;

            for(int i=0;i<taille;i++){
                li[i]=func_card(X, taille, i, continuer);
            }

            printf("Solution pour x = %f    -->    %f\n", continuer, Lagrange(taille, X, Y, continuer));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        int affichePoint;
        printf("Voulez-vous afficher tous les y associes a chaque x pour x allant de 4 a 14 par pas de 0.1 (1 : oui, 0 : non)\n");
        scanf("%d", &affichePoint);
        if(affichePoint == 1){
            int i;
            for(i = 4; i < 15; i += 1){
                printf("%f, ", Lagrange(taille, X, Y, i));
            }
        }
        printf("\n");

        free(li);
    }

    else{
        printf("Erreur recommencer\n");
    }

    free(X);
    free(Y);
}

/* Cette fonction test dans un premier temps l'algo de Neville pour les donnees 
founies, puis pour une ou plusieurs donnees choisie par l'utilisateur et donne
le temps d'execution de l'algorithme.
*/
void testNeville(){
    int time;
    int choix;
    printf("Quel test desirez-vous ?\n");
    scanf("%d",&choix);
    float * X = NULL;
    float * Y = NULL;

    printf("####### NEVILLE #######\n\n");

    if(choix == 1){

        int taille = 20;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 1);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 1);

        afficheDonnees(taille, X, Y);

        printf("Verification sur les valeurs de l'enonce pour Neville : \n\n");
        int i;
        for(i = 0; i < taille; i++){
            time = 0;
            printf("x = %f    -->    Resultat attendu : %f    -->    Solution : %f\n", X[i], Y[i] , Neville(taille, X, Y, X[i]));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        float continuer = 1;
        while(continuer != 0){
            printf("\nEntrez une valeur a tester :\n");
            scanf("%f", &continuer);
            time = 0;
            printf("Solution pour x = %f    -->    %f\n", continuer, Neville(taille, X, Y, continuer));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        int affichePoint;
        printf("Voulez-vous afficher tous les y associes a chaque x pour x allant de 0 a 38 par pas de 0.2 (1 : oui, 0 : non)\n");
        scanf("%d", &affichePoint);
        if(affichePoint == 1){
            float i;
            for(i = 0; i < 38.1; i += 0.2){
                printf("%f, ", Neville(taille, X, Y, i));
            }
        }
        printf("\n");
    }

    else if(choix == 2){
         
        int taille = 21;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 2);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 2);

        afficheDonnees(taille, X, Y);

        printf("Verification sur les valeurs de l'enonce pour Neville : \n\n");
        int i;
        for(i = 0; i < taille; i++){
            time = 0;
            printf("x = %f    -->    Resultat attendu : %f    -->    Solution : %f\n", X[i], Y[i] , Neville(taille, X, Y, X[i]));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        float continuer;
        while(continuer != 0){
            printf("\nEntrez une valeur a tester :\n");
            scanf("%f", &continuer);
            time = 0;
            printf("Solution pour x = %f    -->    %f\n", continuer, Neville(taille, X, Y, continuer));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        int affichePoint;
        printf("Voulez-vous afficher tous les y associes a chaque x pour x allant de 462 a 921 par pas de 1 (1 : oui, 0 : non)\n");
        scanf("%d", &affichePoint);
        if(affichePoint == 1){
            int i;
            for(i = 462; i < 922; i += 1){
                printf("%f, ", Neville(taille, X, Y, i));
            }
        }
        printf("\n");
    }

    else if(choix == 3){

        int taille = 11;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 3);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 3);

        afficheDonnees(taille, X, Y);

        printf("Verification sur les valeurs de l'enonce pour Neville : \n\n");
        int i;
        for(i = 0; i < taille; i++){
            time = 0;
            printf("x = %f    -->    Resultat attendu : %f    -->    Solution : %f\n", X[i], Y[i] , Neville(taille, X, Y, X[i]));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        float continuer;
        while(continuer != 0){
            printf("\nEntrez une valeur a tester :\n");
            scanf("%f", &continuer);
            time = 0;
            printf("Solution pour x = %f    -->    %f\n", continuer, Neville(taille, X, Y, continuer));
            time = clock();
            printf("Temps d'execution : %d ms\n\n", time);
        }

        int affichePoint;
        printf("Voulez-vous afficher tous les y associes a chaque x pour x allant de 4 a 14 par pas de 0.1 (1 : oui, 0 : non)\n");
        scanf("%d", &affichePoint);
        if(affichePoint == 1){
            int i;
            for(i = 4; i < 15; i += 1){
                printf("%f, ", Neville(taille, X, Y, i));
            }
        }
        printf("\n");
    }

    else{
        printf("Erreur recommencer\n");
    }

    free(X);
    free(Y);
}

void testRegressionLineaire(){
    int choix;
    printf("Quel test desirez-vous ?\n");
    scanf("%d",&choix);
    float * X = NULL;
    float * Y = NULL;

    printf("####### REGRESSION LINEAIRE #######\n\n");

    if(choix == 1){

        int taille = 20;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 1);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 1);

        afficheDonnees(taille, X, Y);

        RegressionLineaire(taille, X, Y);
    }

    else if(choix == 2){
         
        int taille = 21;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 2);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 2);

        afficheDonnees(taille, X, Y);

        RegressionLineaire(taille, X, Y);
    }

    else if(choix == 3){

        int taille = 11;
        X = malloc(taille*sizeof(int));
        creerX(X, taille, 3);
        Y = malloc(taille*sizeof(float));
        creerY(Y, taille, 3);

        afficheDonnees(taille, X, Y);

        RegressionLineaire(taille, X, Y);
    }

    else{
        printf("Erreur recommencer\n");
    }

    free(X);
    free(Y);
}

void testApproximationPuissance(){
    printf("Test 4 par defaut\n");
    float * X = NULL;
    float * Y = NULL;

    printf("####### APPROXIMATION PUISSANCE #######\n\n");

    int taille = 7;
    X = malloc(taille*sizeof(int));
    creerX(X, taille, 4);
    Y = malloc(taille*sizeof(float));
    creerY(Y, taille, 4);

    afficheDonnees(taille, X, Y);

    ApproximationPuissance(taille, X, Y);


    free(X);
    free(Y);
}

int main(){
    
    int choix;
    printf("Choisissez la methode a utiliser (Lagrange : 1; Neville : 2; Regression lineaire : 3; Ajustement puissance : 4):\n");
    scanf("%d", &choix);

    if(choix == 1){
        testLagrange();
    }
    
    else if(choix == 2){
        //float X[4] = {0,-5,-1,2};
        //float Y[4] = {-3,12,-4,5};
        testNeville();
        //printf("%f\n", Neville(4,X,Y,3));
    }

    else if(choix == 3){
        testRegressionLineaire();
    }

    else if(choix == 4){
        testApproximationPuissance();
    }

    else{
        printf("ERREUR !!!");
    }
    
    


    return 0;
}
