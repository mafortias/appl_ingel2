#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Cette fonction prend en entree :
- un entier n : taille des tableaux fournis
- un tableau de flottants t1 : tableau dans lequel on va copier les elements
- un tableau de flottants t2 : tableau a partir duquel on va copier les elements
*/
void copieTab(int n, float * t1, float * t2){
    int i;
    for(i = 0; i < n; i++){
        t1[i] = t2[i];
    }
}

float moyTab(int n, float * tab){
    float somme = 0;
    int i;
    for(i = 0; i < n; i++){
        somme += tab[i];
    }
    return somme/n;
}

float produitTab(int n, float * X, float * Y){
    float produit = 0;
    int i;
    for(i = 0; i < n; i++){
        produit += X[i] * Y[i];
    }
    return produit;
}

float sommeTab(int n, float * tab){
    float somme = 0;
    int i;
    for(i = 0; i < n; i++){
        somme += tab[i];
    }
    return somme;
}

float * convertTabLn(int n, float * tab){
    float * tabLn = malloc(n*sizeof(float));
    int i;
    for(i = 0; i < n; i++){
        tabLn[i] = logf(tab[i]);
    }
    return tabLn;
}

float * produitElemTab(int n, float * tab1, float * tab2){
    float * tabCarre = malloc(sizeof(float)*n);
    int i;
    for(i = 0; i < n; i++){
        tabCarre[i] = tab1[i] * tab2[i];
    }

    return tabCarre;
}

/* Cette fonction prend en entree :
- un entier n : taille du tableau fourni
- un tableau de flottants X : abscisses fournies
- un tableau de flottants Y : ordonnees fournies
Cette fonction affiche pour chaque abscisses fournie, son ordonnee associee
*/
void afficheDonnees(int n, float X[], float Y[]){
    int i;
    for(i = 0; i < n; i++){
        printf("%f --> %f\n", X[i], Y[i]);
    }
    printf("\n");
}

//prend en argument une liste X, sa taille et un entier choix
//remplit la liste X des valeurs du tableau correspondant au choix
void creerX(float * X, int taille, int choix){

    if(choix == 1){
        FILE * f1 = fopen("TabX1.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 2){
        FILE * f1 = fopen("TabX2.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 3){
        FILE * f1 = fopen("TabX3.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 4){
        FILE * f1 = fopen("TabX4.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        X[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            X[i]=curseur;
        }
        fclose(f1);
    }
}

//prend en argument une liste Y, sa taille et un entier choix
//remplit la liste Y des valeurs du tableau correspondant au choix
void creerY(float * Y, int taille, int choix){

    if(choix == 1){
        FILE * f1 = fopen("TabY1.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 2){
        FILE * f1 = fopen("TabY2.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 3){
        FILE * f1 = fopen("TabY3.txt","r");

        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }

    else if(choix == 4){
        FILE * f1 = fopen("TabY4.txt","r");
        float curseur;
        fscanf(f1,"%f",&curseur);
        Y[0]=curseur;

        for(int i=1;i<taille;i++){
            fscanf(f1,"%f",&curseur);
            Y[i]=curseur;
        }
        fclose(f1);
    }
}

// cette fonction calcule le resultat de x^y
int puiss(int x, int y){
    int res = 1;
    while(y >= 1){
        res *= x;
        y--;
    }
    return res;
}

// Cette fonction affichera un systeme lineaire ainsi que les valeurs du vecteur X
void AfficheMatrice(int n, float A[][n], float * B, float * X){
    int i;
    for(i = 0; i < n; i++){
        int j;
        for(j = 0; j < n; j++){
            printf("%f ",A[i][j]);
        }
        printf("| %f ", B[i]);
        printf("--> %f", X[i]);
        printf("\n");
    }
    printf("\n");
}

// Cette fonction va effectuer une triangularisation superieur de la matrice 
// representant le systeme lineaire AX = B a l'aide de la methode de Gauss
void triangularisation(int n, float  A[][n], float * B){
    float pivot;

    int k;
    // Parcourt de la premiere colonne a l'avant derniere colonne pour mettre des 0 en-dessous
    // de chaque coefficient
    for(k = 0; k < n-1; k++){
        int i;
        // Parcourt de la matrice de la ligne suivante a la derniere ligne pour calculer le pivot
        // pour chaque ligne
        for(i = k+1; i < n; i++){
            // Calcul du pivot
            pivot = A[i][k] / A[k][k];
            int j;
            // Parcourt de la matrice de la colonne actuelle a la derniere colonne pour appliquer
            // la fonction avec le pivot sur chaque valeur de la ligne            
            for(j = k; j < n; j++){
                // Modification de la valeur de a(ij) de A a partir du pivot
                A[i][j] = A[i][j] - pivot * A[k][j];
            }
            // Modification de la valeur de la ligne i de B
            B[i] = B[i] - pivot * B[k];
        }
    }
}

// Cette fonction va effectuer la resolution d'un systeme lineaire a partir
// d'une matrice triangulaire superieur
void Gauss(int n, float A[][n], float * B, float * X){
    // On calcule l'inconnue la plus facile soit celle situee en derniere ligne
    // et derniere colonne
    X[n-1] = B[n-1] / A[n-1][n-1];

    float somme = 0;

    int i;
    // Parcourt de la matrice de l'avant-derniere ligne a la premiere
    for(i = n-2; i >= 0; i--){
        int j;
        somme = 0;
        // Pour chaque colonne, on calcule la somme des produits de la valeur
        // dans A(i,j) et de la valeur dans X(j)
        for(j = i+1; j < n; j++){
            somme += A[i][j] * X[j];
        }
        // On resout l'equation en effectuant le produit de la difference de 
        // la valeur de B(i) et de la somme calculee par la valeur de A(i,i)
        X[i] = (B[i] - somme) / A[i][i];
    }
}

float * resolutionSysteme(int n, float A[][2], float B[]){
    triangularisation(n, A, B);
    float * X = malloc(sizeof(float)*n);
    X[0] = 0; X[1] = 0;
    Gauss(n, A, B, X);
    return X;
}