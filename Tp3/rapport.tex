\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{lmodern}

\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{enumitem}
\usepackage{hyperref}

\usepackage{listingsutf8}

\usepackage{xcolor}
\usepackage{ulem}

\usepackage{systeme}
\usepackage{graphicx}

\theoremstyle{definition}
\newtheorem{theorem}{Théorème}
\newtheorem{definition}{Définition}
\newtheorem{example}{Exemple}
\newtheorem{Rem}{Remarque}

\newcommand{\lien}[1]{\color{blue} \uline{\textcolor{blue}{#1}}}

\title{Ingénierie et Programmation Numérique Interpolation et approximation}
\author{Augustin Quatrefages et Mathéo Fortias}
\date{Octobre 2022}

\begin{document}

\maketitle
\setcounter{tocdepth}{3}
\tableofcontents


\newpage

\section{Interpolation}

Dans notre cas, l'interpolation consiste à trouver un polynôme $P_{N-1}$ de degré $N-1$ qui vérifie l'ensemble des $N$ points $(x_i, y_i), i = 1, ..., N$ donnés. Voici, les algorithmes de Lagrange et de Neville permettant de trouver ce polynôme.

\subsection{Méthode de Lagrange}

\subsubsection{Rappel rapide de la méthode}

La méthode de Lagrange que nous avons utilisé dans un premier temps est une méthode d'Interpolation abordée dans le cours d'Application en Ingénieurie. Cette méthode consiste à construire le polynôme de degré $N-1$ d'un ensemble de $N$ points par l'expression suivante :

\begin{equation}
P_{(N-1)}(x) = \sum_{i=0}^{(N-1)}y_il_i(x)
\label{eq1}
\end{equation}


Les fonctions "cardinales" $l_i(x)$ sont obtenues pour chaque points donnés par :

\begin{equation}
l_i(x) = \prod_{j=0, j\not=1}^{(N-1)} \frac{x - x_j}{x_i - x_j}
\end{equation}


Ainsi en déclinant cette formule pour chaque degré du polynôme, on obtient en ensemble de $N$ polynômes que l'on va sommer grâce à l'expression (\ref{eq1}) de manière à obtenir l'équation mathématique de la courbe traversant la liste des points donnés.\\
NB : Comme indiqué par notre chargé de TP, notre programme n'affichera pas le polynôme obtenu mais la valeur de ce polynôme en fonction d'une valeur donnée.

\subsubsection{Présentation du programme commenté}

Pour les codes de la méthode Lagrange et de la méthode Neville, nous avons utilisé deux fonctions annexes "creerX" et "creerY" nous permettant de récupérer depuis un fichier les valeurs X et Y données par les jeux d'essais 4.1 4.2 4.3 4.4\\
Ces valeurs seront stockées dans un tableau dynamique remplit selon le jeu d'essai dont nous auront besoin.

Pour le code de la méthode de Lagrange, nous avons créé deux fonctions annexes : la fonction "func card" qui renvoie la valeur pour un $x$ donné de la fonction cardinale $i$. Et la fonction "P" qui renvoie la somme des valeurs de chaque fonction cardinales $i$ (en fonction d'un $x$ donné) multiplié par un $y_i$ donné. Ces fonctions utilisent simplement une boucle for implémentant respectivement un produit et une somme qui seront renvoyées.\\

Enfin pour chaque jeu d'essai choisit le code fonctionnera comme suit :

- récupération dans un tableau dynamique X des valeurs x du jeu d'essai correspondant.

- récupération dans un tableau dynamique Y des valeurs y du jeu d'essai correspondant.

- calcul des valeurs des fonctions cardinales pour le x choisit et assignation de ces valeurs à la liste "li" (fonction func card).

- calcul et affichage de la somme (\ref{eq1}) grâce aux valeurs que nous avons récupérées ou calculées plus tôt (fonction P). 

\subsubsection{Analyse des résultats}

Lorsque l'on lance le programme pour les valeurs de x données dans les jeux d'essais, on remarque que les résultats sont égaux avec les valeurs de y correspondantes fournies. On peut donc conclure que le polynôme obtenu est en adéquation avec les jeux d'essais. Lorsque l'on utilise le programme pour une valeur x qui n'est pas présente dans les jeux d'essais, on obtient une valeur y qui correspond au résultat du polynôme en fonction de la valeur x souhaitée. Si l'on ajoutait cette valeur x avec sa valeur y associée au jeu d'essai, le polynôme obtenu par la méthode de Lagrange serait le même. Si l'on ajoutait cette valeur x avec une valeur y associée arbitraire non-obtenue par le polynôme, le nouveau polynôme généré par la méthode de Lagrange serait différent.\\
Grâce à la fonction "clock()" de la bibliothèque c "time.h" nous avons pu afficher la durée d'exécution de notre programme pour chaque valeur y calculée. Ce temps se compte en ms pour toutes les valeurs calculées et n'est pas influencé par la grandeur des valeurs en jeux. Pour ce qui est du coût en place, notre implémentation de la méthode de Lagrange utilise trois tableaux dynamiques de mêmes tailles (égales au nombres de valeurs du jeu d'essai). L'espace mémoire alloué à ces tableaux est libéré dès que nous n'utilisons plus les valeurs. Ainsi on obtient que le coût en mémoire est environ égal à 3 fois le nombre de valeurs du jeu d'essai concerné fois la taille d'un octet. Soit $3*(nbrVal*8)$ octets.


\subsection{Méthode de Neville} 

\subsubsection{Rappel rapide de la méthode}

La méthode de Neville est un algorithme d'interpolation. A partir de $N$ points du plan donnés, on va trouver un polynôme $P_{N-1}[x_1, x_N]$ de degré $N-1$. Ce polynôme sera obtenu à partir des polynômes $P_{N-2}[x_1, x_{N-1}]$ et $P_{N-2}[x_2, x_N]$. Et ainsi de suite... 
\\

	Ainsi, à l'itération $k = 0$, on aura $N-1$ polynômes de la forme : 
\begin{equation}
P_0[x_i] = y_i, i = 1, ..., N
\end{equation}
où $(x_i, y_i)$ est un point donné. 
\\

	A l'itération $k = 1$, on aura $N-2$ polynômes de la forme :
\begin{equation}
P_1[x_i, x_{i+1}] = \frac{(x-x_{i+1})P_0[x_i](x)+(x_i-x)P_0[x_{i+1}](x)}{x_i-x_{i+1}}, i = 1, ..., N-1
\end{equation}
Or, on connaît déjà $P_0[x_i](x)$ et $P_0[x_{i+1}](x)$, ils ont été calculés à l'itération $k - 1 = 0$.
\\

	A l'itération $k = 2, .., N-1$, on aura $N-k-1$ polynômes de la forme :
\begin{equation}
P_1[x_i, x_{i+k}] = \frac{(x-x_{i+k})P_{k-1}[x_i, x_{i+k-1}](x)+(x_i-x)P_{k-1}[x_{i+1}, x_{i+k}](x)}{x_i-x_{i+k}}
\end{equation}
De la même façon, on connaît déjà $P_{k-1}[x_i, x_{i+k-1}](x)$ et $P_{k-1}[x_{i+1}, x_{i+k}](x)$, ils ont été calculés à l'itération $k - 1$.
\\

On arrive ainsi au polynôme voulu $P_{N-1}[x_1, x_N]$ de degré $N-1$, l'algorithme comprend donc $N-1$ itérations.

\subsubsection{Présentation du programme}
	Comme il nous l'a été précisé par notre chargé de TP, l'objectif ici n'est pas de créer un programme qui renverra le polynôme voulu. Il s'agit plutôt ici de créer un programme en entrant une valeur $x_{voulu}$. Le programme renverra alors la valeur $y_{voulu}$, correspondant au résultat du polynôme en substituant l'inconnue $x$ par la valeur $x_{voulu}$ dans chaque polynôme intermédiare. Ce $y_{voulu}$ est calculé en fonction des points $(x_i, y_i), i = 1, ..., N$ donnés.
	
	Notre programme s'appuie sur la création de deux tableaux de flottants $yTemp$ de taille $N$, et $yTempSuiv$ de taille $N-1$. Le premier tableau contiendra la valeur des polynômes calculés à l'itération $k-1$. Ainsi, à l'itération $k = 1$, le tableau $yTemp$ contiendra les valeurs $y$ fournies dans les jeux d'éssai, correspondant à l'itération $k = 0$. Le tableau $yTempSuiv$ stockera la valeur de chaque polynôme que l'on calcul à l'itération $k$. Ce tableau $yTempSuiv$ est nécéssaire car pour calculer la valeur d'un polynôme, on utilise les valeurs des polynômes calculées à l'itération $k-1$ (contenues dans $yTemp$) et non pas celles calculées à l'itération actuelle $k$ (contenues dans $yTempSuiv$). Ainsi, à la fin de chaque itération, on va copier le contenu de $yTempSuiv$ dans $yTemp$ et donc les tailles de $yTemp$ et de $yTempSuiv$ vont diminuer de 1 à chaque itération .Et ce jusqu'à ce que $yTemp$ ne contienne plus qu'un élément qui correspondra alors à la valeur $y_{voulu}$ associée à la valeur $x_{voulu}$.
\\

A chaque itération, on va donc caluler chaque nouveau polynôme comme ceci. 
\\
Si on est à l'itération $k = 1$, alors on applique cette formule  et on stock les résultats des polynôme dans $yTempSuiv$ :
\begin{equation}
P_1[x_i, x_{i+1}] = \frac{(x-x_{i+1})P_0[x_i](x)+(x_i-x)P_0[x_{i+1}](x)}{x_i-x_{i+1}}, i = 1, ..., N-1
\end{equation}
où $P_1[x_i, x_{i+1}]$ correspond à $yTempSuiv[i]$, $x$ correspond à la valeur $x_{voulu}$ et, $x_i$ et $x_{i+1}$ sont les valeurs contenus dans les données fournies par les jeux d'éssai. $P_0[x_i](x)$ correspondant à $yTemp[i]$ et $P_0[x_{i+1}](x)$, correspondant à $yTemp[i+1]$, sont enfaite les valeur des polynôme qui ont été calculés à l'itération $k - 1 = 0$ et donc qui se trouvent dans le tableau $yTemp$.
\\
Sinon, on applique cette formule :
\begin{equation}
P_k[x_i, x_{i+k}] = \frac{(x-x_{i+k})P_{k-1}[x_i, x_{i+k-1}](x)+(x_i-x)P_{k-1}[x_{i+1}, x_{i+k}](x)}{x_i-x_{i+k}}
\end{equation}
où $P_1[x_i, x_{i+k}]$ correspond à $yTempSuiv[i]$, $x$ correspond à la valeur $x_{voulu}$ et, $x_i$, $x_{i+k-1}$, $x_{i+k}$ et $x_{i+k}$ sont les valeurs contenus dans les données fournies par les jeux d'éssai. $P_{k-1}[x_i, x_{i+k-1}](x)$ correspondant à $yTemp[i]$ et $P_{k-1}[x_{i+1}, x_{i+k}](x)$, correspondant à $yTemp[i+1]$, sont enfaite les valeur des polynôme qui ont été calculés à l'itération $k - 1$ et donc qui se trouvent dans le tableau $yTemp$.

N.B. : la taille des tableaux ne change pas, les cases inutilisées sont simplement remplies par des 0.

\subsubsection{Analyse des résultats}

L'adéquation de la fonction de Neville aux jeux d'essais est vérifiée. En effet, lorsqu'on rentre un $x$ présent dans le tableau donné dan sles jeux d'éssai, on obtient bien le y associé. Par exemple, pour le jeu d'essais 1, si la température de l'eau est de 28 degré, alors la densité devrais être de 0.99664. Or, si on entre $x = 28$ dans la fonction Neville, alors le résultat renvoyé est bien $y = 0.99664$. Ceci, s'explique par le fait que le polynôme calculé par la méthode de Neville est celui qui vérifie l'ensemble des points donnés. Donc en calculant la valeur $y$ du polynôme à partir d'un $x$ donné, on obtient forcément la valeur de $y$ associée à ce $x$ dans le tableau de données. La fonction est en adéquation avec l'ensemble des jeux d'éssai.
\\

Parlons maintenant de l'influence de la modification d'un point donné. Rappelons nous de l'exercice 3 du TD3. On avait trouvé que le polynôme associé aux différents points donnés était $x^{2}+2x-3$. Or, en prenant $x = 3$ en entrée de notre fonction Neville, qui ne correspond pas à un point donné, on trouve bien $y = 12$. En effet, on a bien $3^{2}+2*3-3 = 12$.
Par contre, si on modifie un point donné, par exemple, si on remplace le point $(2, 5)$ par le point $(4, 5)$, on obtient $3.466667$, ce qui n'est pas du tout le même résultat. On en conclut que le fait de modifié un ou plusieurs points donnés change le résultat $y$, alors qu'on part du même $x$. Ceci implique que le polynôme change aussi. On remarque aussi qui si on avait remplaçait le point $(2, 5)$ par le point $(3, 5)$, en prenant $x = 3$, on aurait obtenu $y = 5$ car le polynôme aurait été calculé de sorte à passer par le point $(3, 5)$.
\\

N'ayant pas trouvé d'outil pour evaluer le coût en place, on va analyser ceci de nous même. Globalement, notre fonction de Neville, créer 2 tableaux dynamiques, l'un de la taille $N$ où $N$ est le nombre de points fournis et l'autre de taille $N-1$. C'est alors la seule façon la plus optimale en place qu'on a trouvé jusque-là. En effet, les valeurs de chaque polynômeau rang $k-1$ sont stockées et donc réutilisables sans avoir à être recalculées à chaque itérations. De plus, ce sont les mêmes tableaux qui servent de stockage à chaque itération. Ainsi, notre programme nécéssite en place environ 2 fois le nombre de points fournis (multiplié par la taille en octet d'un float).

Concernant l'evaluation du coût en temps, on a utilisé la fonction clock() de la bibliothèque time.h, grâce à celle-ci, on peut avoir le temps d'éxécution de notre programme, et pour chaque appel de celui-ci, le temps d'éxécution est d'environ 950ms ce qui est très faible. De plus, on remarque que ce temps d'éxécution ne varie pas ou très peu (à quelques ms près) en fonction de la longueur des tableaux de données fournis. Ceci paraît donc quasi-optimal. En effet, le fait de conserver le résultat de chaque polynômes à l'itération $k-1$ permet d'économiser énormément de calculs et donc énormément de temps.

\subsection{Comparaison des deux méthodes}

\includegraphics[scale=0.70]{Figure_5.png}

\includegraphics[scale=0.70]{Figure_6.png}

\includegraphics[scale=0.70]{Figure_7.png}

	Pour comparer ces deux méthodes nous allons étudier l'unicité des polynômes. Pour cela, nous avons généré les courbes représentatives des points obtenus pour chaque jeux d'essais pour chaque méthode. 
	Pour le jeu d'essai 1 : nous avons générer les y associés a chaque x pour x allant de 0 a 38 par pas de 0.2. On remarque que les courbesde chacun des deux polynomes sont quasiment confondues, à l'exception de certaines parties. 
	Pour le jeu d'essai 2 :  nous avons générer les y associés a chaque x pour x allant de 462 a 921 par pas de 1. A l'oeil nu, les courbes représentatives du polynome de Lagrange et du polynome de Neville sont totalement confondues.
	Pour le jeu d'essai 3 :  Nous avons générer les y associés a chaque x pour x allant de 4 a 14 par pas de 0.1. Les courbes ne sont pas confondues mais suivent la même croissance.
	
	Ici, on peut remarquer que les courbes ne sont pas confondues quand le pas choisi n'est pas un entier. On en conclut qu'il doit y avoir une erreur d'arrondi quelque part dans notre fonction de Lagrange qu'on arrive pas à supprimer.
	

\section{Méthode d'Approximation}

	Contrairement à l'interpolation, l'approximation consiste à trouver une fonction ou une équation de droite qui approche au mieux l'ensemble des points donnés. C'est-à-dire que l'objectif va être que la distance moyenne entre les $n$ points donnés et la courbe soit minimale.

\subsection{La Régréssion Linéaire}

\subsubsection{Rappel rapide de la méthode}
	
	La régréssion linéaire est une méthode d'approximation. En effet, dans le cas de la régréssion linéaire, l'approximation se fait par une droite représentative du polynôme de degré 1. Ce polynôme donne donc une équation de droite de degré 1 de la forme $y = a_1 * x + a_0$. Ici, $a_1$ est obtenu comme ceci :

\begin{equation}
a_1 = \frac{(x.y)_{moy} - x_{moy}.y_{moy}}{(x^2)_{moy} - x_{moy}^2}
\end{equation}
où $(x.y)_{moy}$ représente la moyenne des produits de $x$ et de $y$, $x_{moy}.y_{moy}$ représente le produit des moyennes de $x$ et $y$, $(x^2)_{moy}$ représente la moyenne des carrés de $x$ et $x_{moy}^2$ représente la moyenne des $x$ au carré.
\\

et $a_0$ est obtenu comme ceci :

\begin{equation}
a_1 = y_{moy} - a_1 * x_{moy}
\end{equation}
où $y_{moy}$ représente la moyenne des $y$ et $x_{moy}$ représente la moyenne des $x$.
	
	
\subsubsection{Présentation du programme}
	
	A partir d'un tableau de points du plan fourni, l'objectif de la fonction $RegressionLineaire()$ est donc d'afficher une équation de droite de la forme $y = a_1 * x + a_0$ qui approche au mieux les points donnés. On fait donc appel aux deux formules vues ci-dessus pour trouver la valeur de $a_1$ et $a_0$. Pour cela, on va dans un premier temps calculer $xMoy$ correspondant à la moyenne des $x$ et $yMoy$ correspondant à la moyenne des $y$. On calcul ensuite $prodMoy$ le produit de ces deux moyennes. On fait de même avec $carreMoyX$ représentant le carré des moyennes de $x$. Pour calculer $moyProd$ correspondant à la moyenne des produits de $x$ et de $y$, on fait appel à la fonction $produitTab()$ qui renvoie la somme des produits de chaque élément de $x$ de $y$ et on divise ceci par le nombre d'éléments pour obtenir la moyenne. On fait de même avec $moyCarreX$ représentant la moyenne des $x$ au carré.
\\
Il ne nous reste plus qu'à appliquer les formules du cours pour trouver $a_1$ et $a_0$. Enfin, on affiche alors, à l'aide d'un $printf()$, l'équation de la droite approchant au mieux l'ensemble des points donnés.
	
\subsubsection{Analyse des résultats}

En substituant $x$ dans cette équation le $x$ par un $x$ donné, on ne trouve bien évidemment pas le $y$ associé dans le jeu d'éssai car la droite représentée par l'équation ne passe pas par tous les points donnés (ce ne serait pas une droite sinon). Cependant, cette droite approche au mieux l'ensemble des points donnés. La meilleure façon de tester nos résultats est donc de créer des graphiques avec l'équation de chaque droite et l'ensemble des points fournis.

Voici les différents graphiques associés aux jeux d'éssai 4.1, 4.2 4.3 qui permettent de tester notre fonction $RegressionLineaire()$ :

\includegraphics[scale=0.70]{Figure_1.png}

\includegraphics[scale=0.70]{Figure_2.png}

\includegraphics[scale=0.70]{Figure_3.png}

Sur ces graphiques, on retrouve en rouge l'ensemble des points fournis, et en bleu la droite ayant pour équation celle qu'on a trouvé pour les différents jeux d'éssai. On remarque bien sur ces graphiques que la droite semble passer au milieu de tout les points. C'est-à-dire que la distance moyenne entre la droite et chacun des points est minimale. Le graphique représentant au mieux ceci semble être celui du jeu d'ééssai 4.3. En effet, on remarque que le point le plus éloigné se situant au-dessus de la droite semble être à peu près à la même distance que le point éloigné se situant en-dessous de la droite. On remarque aussi que, à un point près, on a le même nombre de points au-dessu et en-dessous de la droite. On comprend ici pourquoi on a utilisé les moyennes pour trouver les équations de droites de chaque jeu d'éssai. On peut aussi dire que le fait d'utliser une droite peut paraître contraignant. En effet, on remarque sur le graphique du jeu d'éssai 4.2 que, bien que la droite approche au mieux l'ensemble des points, ça reste une droite. C'est-à-dire que la majorité des points sont quand même bien éloignés de la droite.

\subsection{L'Ajustement de Puissance}

\subsubsection{Rappel rapide de la méthode}
	
	L'ajustement de puissance est aussi une méthode d'approximation. En effet, on a vu que la régréssion linéaire nous donné une équation de droite approchant au mieux l'ensemble des points donnés. Mais ce n'est qu'une droite, ça reste donc compliqué d'approcher le plus possible les points, c'est-à-dire de trouver une distance moyenne entre chaque point et la courbe qui soit minimale.
\\
	C'est pourquoi la méthode d'ajustement de puissance est intéréssante. En effet, l'objectif de cette méthode est de trouver, de la même façon que pour la régréssion linéaire, une équation de droite qui approche au mieux l'ensemble des points donnés. Cependant, pour la méthode d'ajustement de puissance, l'équation aura la forme suivante : $y = A * x^{a_1}$ où $A = exp(a_0)$. Ansi, la courbe aura donc une allure exponentielle, ce qui sera plus performant qu'une droite de régréssion linéaire dans la majorité des cas. En appliquant le logarithme népérien sur cette équation, on obtient $ln(y) = a_1 * ln(x) + ln(A)$ où $ln(A) = a_0$. En posant $ln(Y) = Y$, $ln(x) = X$ et $ln(A) = a_0$, on obtient une équation de la forme $Y = a_1 * X + a_0$. On remarque que cette équation est de la même forme que celle de la régréssion linéaire. En effet, on va ensuite trouver $a_1$ et $a_0$ de la même façon que pour la régréssion linéaire. Cependant, il faudra en plus calculer $exp(a_0)$ pour retrouver la valeur de $A$ et ainsi obtenir l'équation de départ qui était $y = A * x^{a_1}$.
	
\subsubsection{Présentation du programme}

	A partir d'un tableau de points du plan fourni, l'objectif de la fonction $ApproximationPuissance()$ est donc d'afficher une équation de courbe de la forme $y = A * x^{a_1}$ qui approche au mieux les points donnés.
\\
	Dans un premier temps, il sera nécéssaire, au vu de la description de la méthode ci-dessus, de calculer le logarithme népérien de chaque valeur des tableaux de $x$ et $y$. On va donc créer deux tableaux $tabLnX$ et $tabLnY$ contenant ces logaritmes népériens de $x$ et $y$. Ensuite, on va procéder de la même façon qu'avec la fonction $RegressionLineaire()$ mais en travaillant cette fois-ci sur ces tableaux de logaritme népérien, et non pas sur les tableaux de points fournis. En effet, l'objectif est de trouver $a_1$ et $a_0$ tels que $Y = a_1 * X + a_0$. 
\\
On va d'abord calculer $sommeLnX$ et $sommeLnY$ correspondant respectivement à la somme des logarithmes népérien de $x$ et de $y$. On va ensuite calculer $sommeCarreLnX$ correspondant à la somme des carrés des logaritmes népériens de $x$. Et enfin, on va calculer $sommeProduitLnXLnY$, c'est la somme des produits des logarithmes népérien de $x$ et de $y$. On obtient le système suivant :

\begin{equation}
\systeme{a * a_1 + b * a_0 = c, a' * a_1 + b' * a_0 = c'}
\end{equation}
où a = $sommeCarreLnX$, b = a' = $sommeLnX$, c = $sommeProduitLnXLnY$, b' = $nbElem$ soit le nombre de points fournis et c' = $sommeLnY$.
\\
On résout ce système à l'aide de la méthode de Gauss implémentée lors de la partie 1 du TP1. On obtient ainsi les valeurs de $a_0$ et $a_1$ qu'on retrouve dans l'équation $Y = a_1 * X + a_0$. Il ne nous reste plus qu'à afficher l'équation de la courbe en prenant soint d'affecter à $A$ la valeur de $exp(a_0)$. Pour rappel, cette équation est de la forme $y = A * x^{a_1}$ où $A = exp(a_0)$.
\\

On aurait pu ici utiliser la méthode de régréssion linéaire adaptée aux logarithme népérien. Cependant, on a trouvé intérréssant le fait de résoudre le système obtenu par la methode de Gauss implémentée lors du premier TP plutôt que de reprendre la méthode de régréssion linéaire implémentée précédemment.

\subsubsection{Analyse des résultats}

Il nous a été demandé de tester notre fonction $ApproximationPuissance()$ sur le jeu d'éssai 4.4. Et notre foction nous donne comme équation de courbe approchant au mieux ces points fournis la suivante : $y = 696618.255836 * x^{-2.527194}$.

De la même façon que pour la régréssion linéaire, en substituant $x$ dans cette équation le $x$ par un $x$ donné, on ne trouve bien évidemment pas le $y$ associé dans le jeu d'éssai car la courbe représentée par l'équation ne passe pas par tous les points donnés. Cependant, cette courbe approche au mieux l'ensemble des points donnés. La meilleure façon de tester nos résultats est donc de créer des graphiques avec l'équation de la courbe et l'ensemble des points fournis par le jeu d'éssai 4.4.

Voici donc le graphique associé au jeu d'éssai 4.4 qui permet de tester notre fonction $ApproximationPuissance()$ :


\includegraphics[scale=0.70]{Figure_4.png}

Comme pour la régréssion linéaire, sur ce graphique, on retrouve en rouge l'ensemble des points fournis, et en bleu la courbe ayant pour équation celle qu'on a trouvé pour le jeu d'éssai 4.4, c'est-à-dire $y = 696618.255836 * x^{-2.527194}$. On remarque bien sur ce graphique que la courbe approche au mieux l'ensemble des points fournis. On dirait même que la courbe passe par l'ensemble des points fournis. Mais ce n'est qu'une illusion. En effet, en zoomant sur un des points, on remarque de suite que ce point ne se situe pas sur la courbe. La méthode d'approximation de puissance semble alors parfaitement adaptée à ce jeu d'éssai 4.4. Mais n'est-ce pas parceque la suite de points fournis se rapproche déjà de base à une courbe exponentielle ? En effet, on remarque que si on relie l'ensemble des points fournis, alors on aura quelquechose qui se rapprochera d'une courbe exponentielle.

Ainsi, on peut dire que si la suite de points semble à peu près représenter une courbe exponentielle, alors la méthode d'ajustement de puissance semble être la plus performante. Cependant, si la suite de points semble divergée, alors elle ne sera pas plus performante qu'une autre méthode d'approximation.

\section{Conclusion Générale}

Ainsi, durant ce TP, nous avons eu l'occasion d'implémenter les méthodes de Lagrange et de Neville. Ce sont deux méthodes d'Interpolation. C'est à dire que l'objectif, à partir de n points donnés est de déterminer un polynôme qui satisfait l'ensemble de ces points. Après avoir implémenté ces deux méthodes, nous avons pu remarquer l'adéquation de chacunes d'elles aux jeux d'essais. Nous avons aussi pu tester l'unicité des polynômes obtenus par les deux méthodes. Nous avons également remarqué l'influence de la modification d'un ou plusieurs points donnés. Enfin nous avons remarqué que chacunes des deux méthodes possèdent un faible coût en place et temps.
Nous avons également eu l'occasion d'implémenter la méthode de régression linéaire et la méthode d'approximation de puissances. Ce sont deux méthodes d'approximation, c'est à dire que l'objectif est d'obtenir une équation de la forme $y=f(x)$ qui approche au mieux l'ensemble des points donnés. La première des deux méthodes est celle de régression linéaire. Ici l'équation recherchée est de la forme $y=ax+b$. La deuxième méthode est celle d'approximation puissance, l'équation recherchée est de la forme $y=ax^b$. Nous avons pu remarque l'efficacité de chacunes d'elles (à leur niveau) grâce aux graphiques obtenus par les jeux d'essais.

\end{document}
