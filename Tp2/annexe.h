#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// cette fonction calcule le resultat de x^y
int puiss(int x, int y){
    int res = 1;
    while(y >= 1){
        res *= x;
        y--;
    }
    return res;
}

// Cette fonction affichera un systeme lineaire ainsi que les valeurs du vecteur X
void afficheSysteme(int n, float A[][n], float * B, float * X){
    int i;
    for(i = 0; i < n; i++){
        int j;
        for(j = 0; j < n; j++){
            printf("%f ",A[i][j]);
        }
        printf("| %f ", B[i]);
        printf("--> %f", X[i]);
        printf("\n");
    }
    printf("\n");
}

// Cette fonction prend en entree un flottant et renvoie sa valeur absolue
float valeurAbsolue(float x){
    float valeur;
    if(x < 0){
        valeur = 0-x;
    }
    else{
        valeur = x;
    }

    return valeur;
}